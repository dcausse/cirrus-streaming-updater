include:
  - project: "repos/releng/kokkuri"
    file: "includes/images.yaml"
  - project: "repos/ci-tools/sonarqube"
    ref: 'v0.1.0'
    file: "templates/maven-sonar-ci.yml"

stages:
  - build
  - test
  - publish

variables:
  JAVA8_IMAGE: "docker-registry.wikimedia.org/releng/maven-java8:1.0.0-s1"
  JAVA11_IMAGE: "docker-registry.wikimedia.org/releng/maven-java11:1.0.0"
  MVN_CONSUMER_ARTIFACT_ID: "cirrus-streaming-updater-consumer"
  MVN_PRODUCER_ARTIFACT_ID: "cirrus-streaming-updater-producer"

mvn-package:
  stage: build
  parallel:
    matrix:
      - java_version: [$JAVA8_IMAGE, $JAVA11_IMAGE]
  image:
    name: $java_version
    entrypoint: [""]
  variables:
    MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
  cache:
    - key: "maven"
      paths:
        - .m2/repository
  before_script:
    - cat /proc/meminfo
  script:
    - ./mvnw --batch-mode -gs /settings.xml clean verify
  artifacts:
    name: "$CI_JOB_STAGE-$CI_COMMIT_REF_NAME-jars"
    paths:
      - $MVN_CONSUMER_ARTIFACT_ID/target/$MVN_CONSUMER_ARTIFACT_ID-*-jar-with-dependencies.jar
      - $MVN_PRODUCER_ARTIFACT_ID/target/$MVN_PRODUCER_ARTIFACT_ID-*-jar-with-dependencies.jar
    expire_in: 7 days

build-and-run-image:
  extends: .kokkuri:build-and-run-image
  stage: test
  parallel:
    matrix:
      - app_dir: [ $MVN_CONSUMER_ARTIFACT_ID, $MVN_PRODUCER_ARTIFACT_ID ]
  variables:
    BUILD_VARIANT: test
    BUILD_CONTEXT: ./$app_dir/
    KOKKURI_REGISTRY_CACHE: ''
  needs:
    # Only copy *.jar built with Java 8
    - job: "mvn-package: [$JAVA8_IMAGE]"
      artifacts: true
  before_script:
    # Copy the common blubber pipeline spec to the build context
    - cp -r .pipeline $app_dir
    - ./copy-test-config.sh $app_dir/.pipeline/test

build-and-publish-image:
  extends: .kokkuri:build-and-publish-image
  stage: publish
  rules:
    # Required by https://gitlab.wikimedia.org/repos/releng/gitlab-trusted-runner
    - if: $CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED
  parallel:
    matrix:
      - app_dir: [ $MVN_CONSUMER_ARTIFACT_ID, $MVN_PRODUCER_ARTIFACT_ID ]
  variables:
    BUILD_VARIANT: production
    BUILD_CONTEXT: ./$app_dir/
    PUBLISH_IMAGE_NAME: "${CI_PROJECT_NAMESPACE}/${app_dir}"
  needs:
    - "build-and-run-image"
  before_script:
    # Copy the common blubber pipeline spec to the build context
    - cp -r .pipeline $app_dir
  tags:
    # Required by https://gitlab.wikimedia.org/repos/releng/gitlab-trusted-runner
    - trusted
