package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.assertj.core.api.Assertions.assertThatNoException;

import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.ConfigOption;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.consumer.config.ConsumerConfig;

class ConsumerGraphFactoryTest {

    @Test
    void createStreamGraph() {
        assertThatNoException()
                .isThrownBy(
                        () -> {
                            final ParameterTool params =
                                    ParameterTool.fromArgs(
                                            new String[] {
                                                param(ConsumerConfig.PARAM_EVENT_STREAM_CONFIG_URL),
                                                ConsumerGraphFactoryTest.class
                                                        .getResource("/event-stream-config.json")
                                                        .toString(),
                                                param(ConsumerConfig.PARAM_EVENT_STREAM_JSON_SCHEMA_URLS),
                                                ConsumerGraphFactoryTest.class.getResource("/schema_repo").toString(),
                                                param(ConsumerConfig.PARAM_KAFKA_SOURCE_CONFIG) + ".bootstrap.servers",
                                                "localhost:9292",
                                                param(ConsumerConfig.PARAM_KAFKA_SOURCE_CONFIG) + ".group.id",
                                                "test",
                                                param(ConsumerConfig.PARAM_UPDATE_STREAM),
                                                "cirrussearch.update_pipeline.update",
                                                param(ConsumerConfig.PARAM_ELASTICSEARCH_URLS),
                                                "http://localhost:9200"
                                            });
                            final StreamExecutionEnvironment env =
                                    StreamExecutionEnvironment.getExecutionEnvironment();
                            env.getConfig().setGlobalJobParameters(params);

                            ConsumerConfig config = ConsumerConfig.of(params);
                            new ConsumerGraphFactory(env, config).createStreamGraph();
                        });
    }

    @NotNull
    private static String param(ConfigOption<?> name) {
        return "--" + name.key();
    }
}
