#!/bin/sh -x

export LOG4J_LEVEL=INFO

exec /bin/java -classpath 'lib/*:usrlib/*' \
    org.wikimedia.discovery.cirrus.updater.producer.ProducerApplication \
    --event-stream-config-url file:$(pwd)/resources/event-stream-config.json \
    --event-stream-json-schema-urls file:$(pwd)/resources/schema_repo \
    --kafka-source-config.bootstrap.servers PLAINTEXT://localhost:9092 \
    --kafka-source-config.group.id test \
    --fetch-request-timeout 5s \
    --fetch-retry-max-age 15s \
    --fetch-success-topic eqiad.cirrussearch.update_pipeline.update \
    --fetch-failure-topic eqiad.cirrussearch.update_pipeline.fetch_failure \
    --kafka-sink-config.bootstrap.servers PLAINTEXT://localhost:9092 \
    --kafka-sink-config.batch.size 100 \
    --kafka-sink-config.compression.type snappy \
    --kafka-sink-config.linger.ms 2000 \
    --kafka-sink-config.max.request.size 5242880 \
    --kafka-sink-config.request.timeout.ms 5000 \
    --page-change-stream rc0.mediawiki.page_change \
    --article-topic-stream rc0.mediawiki.revision.score.articletopic \
    --draft-topic-stream rc0.mediawiki.revision.score.drafttopic \
    --recommendation-create-stream rc0.mediawiki.revision.recommendation-create \
    --pipeline integration_pipeline \
    --dry-run true
