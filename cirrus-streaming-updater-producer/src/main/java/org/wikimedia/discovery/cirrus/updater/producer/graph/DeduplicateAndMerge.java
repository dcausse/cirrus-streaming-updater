package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.Window;
import org.apache.flink.util.Collector;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

import com.google.common.base.Verify;

/**
 * Deduplicates and merges events from multiple sources.
 *
 * <p>The general goal here is to reduce indexing load on the downstream elasticsearch instances by
 * reducing the set of updates we emit to the fewest possible. In most cases we should be able to
 * emit a single update event per window.
 */
public class DeduplicateAndMerge<W extends Window>
        extends ProcessWindowFunction<InputEvent, InputEvent, Tuple2<String, Long>, W> {
    private static final String UNPREFIXED_PREFIX = "__UNNAMED_GROUPING__";
    /** Sorts events such that the most recent event to the newest revision comes first. */
    private static final Comparator<InputEvent> EVENT_COMPARATOR =
            Comparator.comparing(InputEvent::getRevId, Comparator.reverseOrder())
                    .thenComparing(InputEvent::getEventTime, Comparator.reverseOrder());

    private static final long serialVersionUID = 8544277587261253978L;

    private transient Counter deduplicated;
    private transient Counter merged;

    public static KeySelector<InputEvent, Tuple2<String, Long>> keySelector() {
        // We return an anonymous class, rather than a lambda, because the lambda loses the type
        // information
        return new KeySelector<InputEvent, Tuple2<String, Long>>() {
            @Override
            public Tuple2<String, Long> getKey(InputEvent inputEvent) {
                return Tuple2.of(
                        inputEvent.getTargetDocument().getDomain(), inputEvent.getTargetDocument().getPageId());
            }
        };
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        deduplicated = getRuntimeContext().getMetricGroup().counter("deduplicated");
        merged = getRuntimeContext().getMetricGroup().counter("merged");
    }

    private static String weightedTagPrefix(String tag) {
        int pos = tag.indexOf('/');
        if (pos == -1) {
            // These shouldn't exist in modern streams, but the downstream superdetectnoop plugin
            // accepts un-prefixed tags.
            return UNPREFIXED_PREFIX;
        }
        return tag.substring(0, pos);
    }

    private InputEvent.Update mergeTagsIntoUpdate(
            InputEvent.Update event, List<String> weightedTags) {
        Map<String, String> noopHints = new HashMap<>(event.getNoopHints());
        noopHints.putAll(InputEvent.Update.WEIGHTED_TAGS_HINT);
        Verify.verify(event.getWeightedTags() == null);
        return new InputEvent.Update(noopHints, event.getRawFields(), weightedTags);
    }

    /**
     * Merge many tags updates into a single update.
     *
     * <p>While the tags are provided as a List<String>, when interpreted by the downstream system the
     * tags are grouped by prefix into Map<String, Set<String>>. When applying sequential tags updates
     * the last update to a prefix always overwrites prior updates, meaning each prefix in the merged
     * result must come from a single event, and that event must be the most recent event.
     */
    private List<String> mergeTagUpdates(Collection<InputEvent> tagUpdates) {
        return tagUpdates.stream()
                .filter(
                        event -> {
                            Verify.verify(event.getChangeType() == ChangeType.TAGS_UPDATE);
                            return true;
                        })
                .sorted(EVENT_COMPARATOR)
                .map(
                        event ->
                                event.getUpdate().getWeightedTags().stream()
                                        .collect(groupingBy(DeduplicateAndMerge::weightedTagPrefix)))
                .reduce(
                        new HashMap<String, List<String>>(),
                        (agg, update) -> {
                            update.forEach(agg::putIfAbsent);
                            return agg;
                        },
                        (a, b) -> {
                            throw new UnsupportedOperationException("must be sequential/non-parallel");
                        })
                .values()
                .stream()
                .flatMap(Collection::stream)
                .collect(toList());
    }

    /**
     * Merges multiple revision based index updates.
     *
     * <p>When receiving edit-based events we will likely have the initial edit event along with
     * followup weighted_tags that are generated by other services based on those same edit events.
     * This process merges those tags updates with the initial edit.
     */
    private InputEvent mergeEvents(@Nullable InputEvent primary, List<InputEvent> tags) {
        merged.inc(tags.size() - (primary == null ? 1L : 0L));
        // If we delete the page these tag update events all become no-op's
        if (primary != null && (tags.isEmpty() || primary.getChangeType() == ChangeType.PAGE_DELETE)) {
            return primary;
        } else if (primary == null && tags.size() == 1) {
            return tags.get(0);
        }

        Verify.verify(primary == null || primary.getChangeType() == ChangeType.REV_BASED_UPDATE);
        List<String> mergedTags = mergeTagUpdates(tags);
        if (primary == null) {
            // There isn't an obvious choice for which event is "best", choosing the most recent
            // event is arbitrary.
            primary =
                    tags.stream()
                            .min(EVENT_COMPARATOR)
                            .orElseThrow(
                                    () -> new IllegalArgumentException("At least one event must be provided."));
            primary.setUpdate(InputEvent.Update.forWeightedTags(mergedTags));
        } else {
            primary.setUpdate(mergeTagsIntoUpdate(primary.getUpdate(), mergedTags));
        }
        return primary;
    }

    /**
     * Deduplicates indexing events
     *
     * <p>This is essentially a bypassable reduce operation. Events that cannot be deduplicated are
     * passed through untouched. Events that can are reduced such that only the last event to the
     * highest revision id is emitted. Bypassed events are added to the list provided in parameters,
     * the reduced primary event, or null, is returned. This has the effect of separating out a single
     * nullable primary event, which typically comes from mediawiki, and a list of secondary tags
     * update events to be merged into the primary event.
     *
     * <p>The general goal here is to deduplicate over the action space. Revision based updates from
     * mediawiki only do two things: 1) delete the page 2) replace the page with results of cirrusdoc
     * api. Doing any of those actions twice in a row would be pointless, the second action either
     * does nothing or replaces what the prior action did. Similarly, these actions are inverse of
     * each other. A delete after an update, or indexing a full doc after deleting it are equally
     * pointless. In all cases the last action overrides the prior.
     */
    private InputEvent deduplicate(Iterable<InputEvent> iterable, List<InputEvent> tagsOut) {
        InputEvent current = null;
        for (InputEvent event : iterable) {
            if (event.getChangeType() == ChangeType.TAGS_UPDATE) {
                tagsOut.add(event);
                continue;
            }
            Verify.verify(
                    event.getChangeType() == ChangeType.PAGE_DELETE
                            || event.getChangeType() == ChangeType.REV_BASED_UPDATE);
            if (current == null) {
                current = event;
            } else if (EVENT_COMPARATOR.compare(current, event) > 0) {
                current = event;
                deduplicated.inc();
            } else {
                deduplicated.inc();
            }
        }
        return current;
    }

    @Override
    public void process(
            Tuple2<String, Long> key,
            ProcessWindowFunction<InputEvent, InputEvent, Tuple2<String, Long>, W>.Context context,
            Iterable<InputEvent> iterable,
            Collector<InputEvent> out) {
        List<InputEvent> tags = new ArrayList<>();
        InputEvent primary = deduplicate(iterable, tags);
        out.collect(mergeEvents(primary, tags));
    }
}
