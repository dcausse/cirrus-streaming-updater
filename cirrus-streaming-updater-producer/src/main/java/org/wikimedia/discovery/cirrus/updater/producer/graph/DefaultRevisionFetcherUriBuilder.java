package org.wikimedia.discovery.cirrus.updater.producer.graph;

import java.net.URI;

import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

public class DefaultRevisionFetcherUriBuilder implements RevisionFetcherUriBuilder {
    private static final String MW_API_PATH_AND_QUERY =
            "/w/api.php?action=query&format=json&cbbuilders=content&prop=cirrusbuilddoc&formatversion=2&format=json&revids=";

    @Override
    public URI build(InputEvent inputEvent) {
        return URI.create(
                "https://"
                        + inputEvent.getTargetDocument().getDomain()
                        + MW_API_PATH_AND_QUERY
                        + inputEvent.getRevId());
    }
}
