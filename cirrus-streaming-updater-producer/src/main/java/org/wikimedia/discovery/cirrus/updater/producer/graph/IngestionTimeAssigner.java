package org.wikimedia.discovery.cirrus.updater.producer.graph;

import org.apache.flink.api.common.functions.MapFunction;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.eventutilities.core.SerializableClock;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class IngestionTimeAssigner implements MapFunction<InputEvent, InputEvent> {
    private final SerializableClock clock;

    @Override
    public InputEvent map(InputEvent inputEvent) {
        inputEvent.setIngestionTime(clock.get());
        return inputEvent;
    }
}
