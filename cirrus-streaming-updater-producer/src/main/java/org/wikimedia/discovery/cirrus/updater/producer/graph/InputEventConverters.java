package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static com.google.common.base.Preconditions.checkArgument;
import static org.wikimedia.discovery.cirrus.updater.producer.model.JsonPathUtils.getRequiredNode;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.flink.types.Row;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("checkstyle:HideUtilityClassConstructor")
@SuppressFBWarnings(value = "CE_CLASS_ENVY", justification = "false positive")
public class InputEventConverters {

    private static final String PAGE_CHANGE_SCHEMA = "/mediawiki/page/change";
    private static final String REVISION_SCORE_SCHEMA = "/mediawiki/revision/score";
    private static final String RECOMMENDATION_CREATE_SCHEMA =
            "/mediawiki/revision/recommendation-create";

    private static void checkSchema(Row row, String expectedSchema) {
        final String schema = row.getFieldAs("$schema");
        checkArgument(schema != null, "Undefined source schema");
        checkArgument(schema.startsWith(expectedSchema), "Unknown source schema: " + schema);
    }

    private static void applyEventMeta(Row row, InputEvent event) {
        Row meta = row.getFieldAs("meta");
        event.setRequestId(meta.getFieldAs("request_id"));
        event.setEventStream(meta.getFieldAs("stream"));
        event.setEventTime(meta.getFieldAs("dt"));
    }

    public static InputEvent fromPageChange(Row row) {
        checkSchema(row, PAGE_CHANGE_SCHEMA);
        InputEvent event = new InputEvent();
        applyEventMeta(row, event);

        String pageChangeKind = row.getFieldAs("page_change_kind");

        if ("delete".equals(pageChangeKind)) {
            event.setChangeType(ChangeType.PAGE_DELETE);
        } else {
            event.setChangeType(ChangeType.REV_BASED_UPDATE);
        }

        Row page = row.getFieldAs("page");
        Row meta = row.getFieldAs("meta");
        event.setPageTitle(page.getFieldAs("page_title"));

        Row revision = row.getFieldAs("revision");
        event.setRevId(revision.getFieldAs("rev_id"));
        event.setEventTime(revision.getFieldAs("rev_dt"));

        event.setTargetDocument(
                new InputEvent.TargetDocument(
                        meta.getFieldAs("domain"), row.getFieldAs("wiki_id"),
                        page.getFieldAs("namespace_id"), page.getFieldAs("page_id")));
        return event;
    }

    private static final int SCALE_FACTOR = 1000;
    private static final String TOPIC_SEPARATOR = "/";
    private static final String PROBABILITY_SEPARATOR = "|";

    private static String stringifyPrediction(String prefix, String topic, float prob) {
        return stringifyPrediction(prefix, topic, (int) (prob * SCALE_FACTOR));
    }

    private static String stringifyPrediction(String prefix, String topic, int prob) {
        checkArgument(!topic.contains(PROBABILITY_SEPARATOR), "Topic names must not contain |");
        return prefix + TOPIC_SEPARATOR + topic + PROBABILITY_SEPARATOR + prob;
    }

    private static List<String> preprocessRevisionScoring(Row row, String prefix) {
        Row scores = row.getFieldAs("scores");
        Set<String> modelNames = scores.getFieldNames(false);
        return modelNames.stream()
                .map(scores::<Row>getFieldAs)
                .flatMap(
                        score -> {
                            List<String> topics = score.getFieldAs("prediction");
                            Map<String, Float> probability = score.getFieldAs("probability");
                            return topics.stream()
                                    .map(topic -> stringifyPrediction(prefix, topic, probability.get(topic)));
                        })
                .collect(Collectors.toList());
    }

    public static InputEvent fromRevisionScoring(Row row, String predictionPrefix) {
        checkSchema(row, REVISION_SCORE_SCHEMA);
        InputEvent event = new InputEvent();
        applyEventMeta(row, event);

        Row meta = row.getFieldAs("meta");
        event.setChangeType(ChangeType.TAGS_UPDATE);
        event.setTargetDocument(
                new InputEvent.TargetDocument(
                        row.getFieldAs("database"), meta.getFieldAs("domain"),
                        row.getFieldAs("page_namespace"), row.getFieldAs("page_id")));
        event.setPageTitle(row.getFieldAs("page_title"));
        event.setRevId(row.getFieldAs("rev_id"));
        event.setUpdate(
                InputEvent.Update.forWeightedTags(preprocessRevisionScoring(row, predictionPrefix)));

        return event;
    }

    private static List<String> preprocessRecommendationCreate(Row row) {
        String recommendationType = row.getFieldAs("recommendationType");
        String prefix = "recommendation." + recommendationType;
        // The only information to share about recommendations is if they exist, provided a constant
        // topic and probability to match the use case.
        return Collections.singletonList(stringifyPrediction(prefix, "exists", 1));
    }

    public static InputEvent fromRecommendationCreate(Row row) {
        checkSchema(row, RECOMMENDATION_CREATE_SCHEMA);
        InputEvent event = new InputEvent();
        applyEventMeta(row, event);

        event.setChangeType(ChangeType.TAGS_UPDATE);
        Row meta = row.getFieldAs("meta");
        event.setTargetDocument(
                new InputEvent.TargetDocument(
                        row.getFieldAs("database"), meta.getFieldAs("domain"),
                        row.getFieldAs("page_namespace"), row.getFieldAs("page_id")));
        event.setPageTitle(row.getFieldAs("page_title"));
        event.setRevId(row.getFieldAs("rev_id"));
        event.setUpdate(InputEvent.Update.forWeightedTags(preprocessRecommendationCreate(row)));

        return event;
    }

    /** Augments InputEvent with the cirrusdoc response from CirrusSearch API. */
    public static RevisionFetcher.EventAugmenter cirrusDocAugmenter(ObjectMapper objectMapper) {
        ObjectWriter objectWriter = objectMapper.writer();
        return (InputEvent inputEvent, JsonNode page) -> {
            final JsonNode cirrusBuildDoc = getRequiredNode(page, "/cirrusbuilddoc");
            final JsonNode cirrusBuildDocMetadata = getRequiredNode(page, "/cirrusbuilddoc_metadata");

            Long pageId = getRequiredNode(cirrusBuildDoc, "/page_id").asLong();
            if (!pageId.equals(inputEvent.getTargetDocument().getPageId())) {
                throw new IllegalArgumentException("Received response for wrong page id");
            }
            Long revId = getRequiredNode(cirrusBuildDoc, "/version").asLong();
            if (inputEvent.getRevId() == null) {
                // TODO: Does this do anything? I don't think we use the rev id after fetching
                inputEvent.setRevId(revId);
            } else if (!inputEvent.getRevId().equals(revId)) {
                throw new IllegalArgumentException("Received response for wrong revision id");
            }

            InputEvent.TargetDocument target = inputEvent.getTargetDocument();
            Long namespaceId = getRequiredNode(cirrusBuildDoc, "/namespace").asLong();
            if (!target.getPageNamespace().equals(namespaceId)) {
                // How could namespace disagree? Perhaps the page has moved since the initial event
                // was fired. This api response should be the most recent information, lets set it
                target =
                        new InputEvent.TargetDocument(
                                target.getDomain(), target.getWikiId(), namespaceId, pageId);
                inputEvent.setTargetDocument(target);
            }

            String clusterGroup = getRequiredNode(cirrusBuildDocMetadata, "/cluster_group").asText();
            String indexName = getRequiredNode(cirrusBuildDocMetadata, "/index_name").asText();
            target.completeWith(clusterGroup, indexName);

            final Map<String, String> noopHints =
                    objectMapper.convertValue(
                            getRequiredNode(cirrusBuildDocMetadata, "/noop_hints"),
                            new TypeReference<Map<String, String>>() {});

            final byte[] rawFields = objectWriter.writeValueAsBytes(cirrusBuildDoc);
            inputEvent.setUpdate(new InputEvent.Update(noopHints, rawFields));
            return inputEvent;
        };
    }
}
