package org.wikimedia.discovery.cirrus.updater.producer.graph;

import java.util.Optional;

import lombok.ToString;

@ToString
public class RetryContext<I, O> {

    private final int iteration;
    private final I input;
    private final O output;
    private final Throwable failure;

    /**
     * Default constructor required by {@link
     * org.apache.flink.api.java.typeutils.runtime.PojoSerializer}.
     */
    public RetryContext() {
        this(
                -1,
                null,
                null,
                new IllegalAccessException(
                        "Do not use RetryContext's default constructor, it only exists for PojoSerializer to work."));
    }

    public RetryContext(int iteration, I input, O output, Throwable failure) {
        this.iteration = iteration;
        this.input = input;
        this.output = output;
        this.failure = failure;
    }

    public I getInput() {
        return input;
    }

    public Optional<O> getOutput() {
        return Optional.ofNullable(output);
    }

    public Optional<Throwable> getFailure() {
        return Optional.ofNullable(failure);
    }

    public int getIteration() {
        return iteration;
    }

    static <I, O> RetryContext<I, O> of(int iteration, I input, O output, Throwable failure) {
        return new RetryContext<>(iteration, input, output, failure);
    }
}
