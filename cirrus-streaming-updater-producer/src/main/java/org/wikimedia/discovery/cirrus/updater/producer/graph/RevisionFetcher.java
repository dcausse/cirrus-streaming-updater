package org.wikimedia.discovery.cirrus.updater.producer.graph;

import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleHttpResponse;
import org.apache.hc.client5.http.impl.DefaultHttpRequestRetryStrategy;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.core5.concurrent.FutureCallback;
import org.apache.hc.core5.http.Method;
import org.wikimedia.discovery.cirrus.updater.common.async.CompletableFutureBackPorts;
import org.wikimedia.discovery.cirrus.updater.producer.http.HttpClientFactory;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.BooleanNode;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * A {@link Function function} fetching revision data via HTTP.
 *
 * <p>This function only retries HTTP requests if protocol implies it, for example by responding
 * with a code of <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/429">429</a>,
 * see {@link DefaultHttpRequestRetryStrategy}. Retries implied by the HTTP payload must be
 * implemented at a higher level, for example via {@link RetryingAsyncFunction}. Otherwise, we would
 * have to tell the content-agnostic part of the HTTP stack how to interpret content.
 */
@SuppressFBWarnings("SE_NO_SERIALVERSIONID")
public class RevisionFetcher
        implements Function<InputEvent, CompletionStage<InputEvent>>, Serializable, Closeable {

    private final Duration requestTimeout;

    private final RevisionFetcherUriBuilder urlBuilder;
    private final Map<String, String> httpRoutes;
    private final EventAugmenter augmenter;

    private transient CloseableHttpAsyncClient httpClient;

    private transient ObjectMapper objectMapper;

    @FunctionalInterface
    public interface EventAugmenter extends Serializable {
        InputEvent augmentWithCirrusBuildDoc(InputEvent inputEvent, JsonNode page) throws IOException;
    }

    public RevisionFetcher(
            Duration requestTimeout,
            RevisionFetcherUriBuilder urlBuilder,
            EventAugmenter augmenter,
            Map<String, String> httpRoutes) {
        this.requestTimeout = requestTimeout;
        this.urlBuilder = urlBuilder;
        this.augmenter = augmenter;
        this.httpRoutes = httpRoutes;
    }

    @Override
    public void close() throws IOException {
        if (httpClient != null) {
            httpClient.close();
        }
    }

    @Override
    public CompletionStage<InputEvent> apply(InputEvent inputEvent) {
        final Long revId = inputEvent.getRevId();
        final CompletableFuture<JsonNode> result = new CompletableFuture<>();

        getClient()
                .execute(
                        SimpleHttpRequest.create(Method.GET.name(), urlBuilder.build(inputEvent)),
                        new FetchRevisionCallback(revId, getObjectMapper(), result));

        return result.thenCompose(
                page -> {
                    try {
                        return CompletableFuture.completedFuture(
                                augmenter.augmentWithCirrusBuildDoc(inputEvent, page));
                    } catch (IOException | IllegalArgumentException e) {
                        return CompletableFutureBackPorts.failedFuture(e);
                    }
                });
    }

    private CloseableHttpAsyncClient getClient() {
        if (httpClient == null) {
            httpClient = HttpClientFactory.buildAsyncClient(httpRoutes, requestTimeout);
            httpClient.start();
        }

        return httpClient;
    }

    private ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
        return objectMapper;
    }

    private static final class FetchRevisionCallback implements FutureCallback<SimpleHttpResponse> {

        private final Long revId;

        private final ObjectMapper objectMapper;

        private final CompletableFuture<JsonNode> delegate;

        FetchRevisionCallback(
                Long revId, ObjectMapper objectMapper, CompletableFuture<JsonNode> delegate) {
            this.revId = revId;
            this.objectMapper = objectMapper;
            this.delegate = delegate;
        }

        @Override
        @SuppressWarnings("IllegalCatch")
        public void completed(SimpleHttpResponse httpResponse) {
            // Based on
            // https://github.com/openjdk-mirror/jdk7u-jdk/blob/master/src/share/classes/java/util/concurrent/ThreadPoolExecutor.java#L1109
            // goal is to avoid leaking an exception as we want to notify the delegate that something did
            // not run well.
            // We also want to stop the current thread in case an unexpected problem occurred.
            Throwable thrown = null;
            try {
                delegate.complete(getRevisionData(httpResponse));
            } catch (IOException | RevisionNotFoundException | InvalidMWApiResponseException e) {
                // these are the exception we expect, only notify the delegate
                thrown = e;
            } catch (Throwable e) {
                // every other problems is both propagated to the current thread and the delegate
                thrown = e;
                throw e;
            } finally {
                if (thrown != null) {
                    delegate.completeExceptionally(thrown);
                }
            }
        }

        @Override
        public void failed(Exception e) {
            delegate.completeExceptionally(e);
        }

        @Override
        public void cancelled() {
            failed(new CancellationException());
        }

        private JsonNode getRevisionData(SimpleHttpResponse httpResponse) throws IOException {
            final byte[] json = httpResponse.getBody().getBodyBytes();
            if (httpResponse.getCode() != 200) {
                throw new InvalidMWApiResponseException(
                        "Unexpected response ("
                                + httpResponse.getCode()
                                + ") for revision: "
                                + httpResponse.getReasonPhrase());
            }
            return getRevisionData(json);
        }

        /**
         * Extracts and returns the (only expected) page object from {@code json}.
         *
         * @param json MW API response for {@code cirrusbuilddoc}
         * @return {@code /query/pages/0} the (only expected) page object
         * @throws IOException if parsing {@code json} as JSON fails
         * @throws RevisionNotFoundException if {@code json} indicates a missing revision or does not
         *     hold the expected {@code page[0]}
         */
        private JsonNode getRevisionData(byte[] json) throws IOException, RevisionNotFoundException {
            final JsonNode rootNode = objectMapper.readTree(json);
            final JsonNode missingFlagNode = rootNode.at("/query/badrevids/" + revId + "/missing");
            final JsonNode pageNode = rootNode.at("/query/pages/0");
            if (missingFlagNode instanceof BooleanNode && missingFlagNode.asBoolean()
                    || pageNode.isMissingNode()) {
                throw new RevisionNotFoundException("Revision " + revId + " is missing");
            }
            return pageNode;
        }
    }
}
