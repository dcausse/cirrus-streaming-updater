package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static com.google.common.collect.ImmutableList.of;

import java.io.IOException;
import java.io.Serializable;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletionException;
import java.util.function.Predicate;

import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.eventutilities.core.SerializableClock;

public final class RevisionFetcherRetryPredicate
        implements Predicate<RetryContext<InputEvent, InputEvent>>, Serializable {

    private static final List<Class<?>> RETRYABLE_EXCEPTIONS;

    static {
        RETRYABLE_EXCEPTIONS =
                of(InvalidMWApiResponseException.class, IOException.class, RevisionNotFoundException.class);
    }

    private final Duration retryFetchTimeout;

    private final SerializableClock clock;

    private final int maxRetriesForLateEvents;

    public RevisionFetcherRetryPredicate(
            Duration retryFetchTimeout, SerializableClock clock, int maxRetriesForLateEvents) {
        this.retryFetchTimeout = retryFetchTimeout;
        this.clock = clock;
        this.maxRetriesForLateEvents = maxRetriesForLateEvents;
    }

    @Override
    public boolean test(RetryContext<InputEvent, InputEvent> context) {
        boolean retryable =
                context
                        .getFailure()
                        .map(failure -> failure instanceof CompletionException ? failure.getCause() : failure)
                        .map(Object::getClass)
                        .filter(
                                failure ->
                                        RETRYABLE_EXCEPTIONS.stream().anyMatch(cls -> cls.isAssignableFrom(failure)))
                        .isPresent();

        final boolean inTime =
                context.getInput().getEventTime().plus(retryFetchTimeout).isAfter(clock.get());

        return retryable && (inTime || context.getIteration() < maxRetriesForLateEvents);
    }
}
