package org.wikimedia.discovery.cirrus.updater.producer.graph;

import java.io.Serializable;
import java.net.URI;

import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

public interface RevisionFetcherUriBuilder extends Serializable {

    URI build(InputEvent inputEvent);
}
