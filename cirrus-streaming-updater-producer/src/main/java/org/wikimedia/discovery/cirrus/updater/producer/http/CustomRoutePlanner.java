package org.wikimedia.discovery.cirrus.updater.producer.http;

import java.net.URISyntaxException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.hc.client5.http.HttpRoute;
import org.apache.hc.client5.http.routing.HttpRoutePlanner;
import org.apache.hc.core5.http.HttpException;
import org.apache.hc.core5.http.HttpHost;
import org.apache.hc.core5.http.protocol.HttpContext;

/**
 * wmf-jvm-utils provides http-client-utils which is based on http client v4 sadly this does not
 * match the http client v5 that we need for this project. Put this route planner in here for now
 * but it might be worthwhile to consider putting it in wmf-jvm-utils.
 */
public class CustomRoutePlanner implements HttpRoutePlanner {
    private final Map<String, HttpHost> customRouteMap;
    private final HttpRoutePlanner defaultRoutePlanner;

    public static Map<String, HttpHost> map(Map<String, String> hostUrlMap) {
        return hostUrlMap.entrySet().stream()
                .collect(Collectors.toMap(Entry::getKey, entry -> createHttpHost(entry.getValue())));
    }

    private static HttpHost createHttpHost(String uri) {
        try {
            return HttpHost.create(uri);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public CustomRoutePlanner(
            Map<String, HttpHost> customRouteMap, HttpRoutePlanner defaultRoutePlanner) {
        this.customRouteMap = customRouteMap;
        this.defaultRoutePlanner = defaultRoutePlanner;
    }

    @Override
    public HttpRoute determineRoute(HttpHost httpHost, HttpContext httpContext) throws HttpException {
        HttpHost destHost = customRouteMap.get(httpHost.getHostName());
        if (destHost != null) {
            return new HttpRoute(
                    new HttpHost(
                            destHost.getSchemeName(),
                            destHost.getHostName(),
                            // If the dest port was not set, then assume we want to use the same one as the
                            // request url.
                            destHost.getPort() != -1 ? destHost.getPort() : httpHost.getPort()));
        } else {
            return defaultRoutePlanner.determineRoute(httpHost, httpContext);
        }
    }
}
