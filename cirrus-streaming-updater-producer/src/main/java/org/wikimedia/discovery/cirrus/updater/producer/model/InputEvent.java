package org.wikimedia.discovery.cirrus.updater.producer.model;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
public class InputEvent {
    ChangeType changeType;
    String requestId;
    String eventStream;
    Instant eventTime;
    String pageTitle;
    Long revId;
    Instant ingestionTime;
    TargetDocument targetDocument;
    Update update;

    /** Targeting information for a single document in elasticsearch. */
    @Data
    @RequiredArgsConstructor
    public static class TargetDocument {
        private final String domain;
        private final String wikiId;
        private final Long pageNamespace;
        private final Long pageId;
        private String clusterGroup;
        private String indexName;

        public boolean isComplete() {
            return clusterGroup != null && indexName != null;
        }

        public void completeWith(String clusterGroup, String indexName) {
            this.clusterGroup = clusterGroup;
            this.indexName = indexName;
        }
    }

    @Data
    @AllArgsConstructor
    public static class Update {
        public static final Map<String, String> WEIGHTED_TAGS_HINT =
                Collections.singletonMap("weighted_tags", "multilist");
        private final Map<String, String> noopHints;
        private final byte[] rawFields;
        private final List<String> weightedTags;

        public Update(Map<String, String> noopHints, byte[] rawFields) {
            this.noopHints = noopHints;
            this.rawFields = rawFields;
            this.weightedTags = null;
        }

        public Update(Map<String, String> noopHints, List<String> weightedTags) {
            this.noopHints = noopHints;
            this.rawFields = null;
            this.weightedTags = weightedTags;
        }

        public static Update forWeightedTags(List<String> weightedTags) {
            return new Update(WEIGHTED_TAGS_HINT, weightedTags);
        }
    }
}
