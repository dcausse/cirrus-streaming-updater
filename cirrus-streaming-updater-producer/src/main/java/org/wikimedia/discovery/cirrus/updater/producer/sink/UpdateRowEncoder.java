package org.wikimedia.discovery.cirrus.updater.producer.sink;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.Nonnull;

import org.apache.flink.types.Row;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema.Builder;

public class UpdateRowEncoder implements Serializable {

    private static final long serialVersionUID = -5633212924146308257L;
    private final EventRowTypeInfo outputTypeInfo;
    private final JsonRowDeserializationSchema fieldsSchema;

    public UpdateRowEncoder(EventRowTypeInfo outputTypeInfo) {
        this.outputTypeInfo = outputTypeInfo;
        this.fieldsSchema = new Builder(outputTypeInfo.getTypeAt("fields")).build();
    }

    @Nonnull
    public Row encodeInputEvent(InputEvent inputEvent) throws IOException {
        final Row row = outputTypeInfo.createEmptyRow();
        final InputEvent.TargetDocument target = inputEvent.getTargetDocument();
        if (!target.isComplete()) {
            throw new IllegalArgumentException("TargetDocument is incomplete");
        }

        row.setField(UpdateFields.OPERATION, asOperation(inputEvent));

        // TODO: pass through the mediawiki request_id for debugging outputs
        row.setField("dt", inputEvent.getEventTime());
        row.setField("domain", target.getDomain());
        row.setField("wiki_id", target.getWikiId());

        row.setField(UpdateFields.INDEX_NAME, target.getIndexName());
        row.setField(UpdateFields.PAGE_ID, target.getPageId());
        row.setField(UpdateFields.PAGE_NAMESPACE, target.getPageNamespace());
        row.setField(UpdateFields.REV_ID, inputEvent.getRevId());
        row.setField("cluster_group", target.getClusterGroup());

        InputEvent.Update update = inputEvent.getUpdate();
        if (update == null) {
            if (inputEvent.getChangeType() == ChangeType.PAGE_DELETE) {
                return row;
            }
            throw new IllegalArgumentException("No update provided to non-delete operation");
        }

        row.setField(UpdateFields.NOOP_HINTS, update.getNoopHints());
        final Row fields = fieldsSchema.deserialize(update.getRawFields());
        if (update.getWeightedTags() != null) {
            // we don't attempt to merge with an existing value as this should never
            // be provided by cirrusdoc.
            fields.setField("weighted_tags", update.getWeightedTags());
        }
        row.setField(UpdateFields.FIELDS, fields);

        return row;
    }

    private static String asOperation(InputEvent inputEvent) {
        switch (inputEvent.getChangeType()) {
            case PAGE_DELETE:
                return UpdateFields.OPERATION_DELETE;
            case REV_BASED_UPDATE:
                return UpdateFields.OPERATION_UPDATE_REVISION;
            case TAGS_UPDATE:
                return UpdateFields.OPERATION_PARTIAL_UPDATE;
            default:
                final InputEvent.TargetDocument target = inputEvent.getTargetDocument();
                throw new IllegalArgumentException(
                        "No operation mapping for page-change-type "
                                + inputEvent.getChangeType()
                                + " of page-id "
                                + target.getPageId()
                                + " in "
                                + target.getWikiId());
        }
    }
}
