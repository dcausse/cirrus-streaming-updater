package org.wikimedia.discovery.cirrus.updater.producer.config;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;

import org.apache.flink.api.java.utils.ParameterTool;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;
import org.testcontainers.shaded.com.google.common.collect.ImmutableMap;

class ProducerConfigTest {

    final String streamConfig = "https://streamconfig.local";
    final Duration fetchRequestTimeout = Duration.ofSeconds(20);
    final Duration retryFetchTimeout = Duration.ofSeconds(30);
    final String pageChangeStream = "rc0.mediawiki.page_change";
    final String kafkaSourceBootstrapServers = "kafka-source.local:4242";
    final String kafkaSourceGroupId = "test";
    final String kafkaSinkBootstrapServers = "kafka-sink.local:4242";
    final Instant kafkaStartTime = Instant.now();
    final Instant kafkaEndTime = Instant.now();
    final String jsonSchemaUrl = "http://localhost:8080/json-schema";
    final String pipeline = "my_pipeline";
    final String fetchFailureTopic = "fetch_failure_topic";
    final String fetchSuccessTopic = "fetch_success_topic";

    final Map<String, String> httpRoutes = ImmutableMap.of("test.local", "https://other.remote:4242");

    @Test
    void fromArgs() {
        final ProducerConfig config =
                ProducerConfig.of(
                        ParameterTool.fromArgs(
                                new String[] {
                                    "--event-stream-config-url",
                                    streamConfig,
                                    "--event-stream-json-schema-urls",
                                    jsonSchemaUrl,
                                    "--fetch-request-timeout",
                                    fetchRequestTimeout.getSeconds() + "s",
                                    "--fetch-retry-max-age",
                                    retryFetchTimeout.getSeconds() + "s",
                                    "--page-change-stream",
                                    pageChangeStream,
                                    "--article-topic-stream",
                                    "unused-empty-stream",
                                    "--draft-topic-stream",
                                    "unused-empty-stream",
                                    "--recommendation-create-stream",
                                    "unused-empty-stream",
                                    "--kafka-source-config.bootstrap.servers",
                                    kafkaSourceBootstrapServers,
                                    "--kafka-source-config.group.id",
                                    kafkaSourceGroupId,
                                    "--kafka-source-start-time",
                                    kafkaStartTime.toString(),
                                    "--kafka-source-end-time",
                                    kafkaEndTime.toString(),
                                    "--kafka-sink-config.bootstrap.servers",
                                    kafkaSinkBootstrapServers,
                                    "--kafka-sink-config.linger.ms",
                                    "42",
                                    "--kafka-sink-config.batch.size",
                                    "111",
                                    "--pipeline",
                                    pipeline,
                                    "--fetch-failure-topic",
                                    fetchFailureTopic,
                                    "--fetch-success-topic",
                                    fetchSuccessTopic,
                                    "--http-routes.test.local",
                                    httpRoutes.get("test.local")
                                }));

        assertThat(config.eventStreamConfigUrl()).isEqualTo(streamConfig);
        assertThat(config.eventStreamJsonSchemaUrls()).isEqualTo(singletonList(jsonSchemaUrl));
        assertThat(config.retryFetchMaxAge()).isEqualTo(retryFetchTimeout);
        assertThat(config.fetchRequestTimeout()).isEqualTo(fetchRequestTimeout);
        assertThat(config.pageChangeStream()).isEqualTo(pageChangeStream);
        assertThat(config.kafkaSourceBootstrapServers()).isEqualTo(kafkaSourceBootstrapServers);
        assertThat(config.kafkaSinkBootstrapServers()).isEqualTo(kafkaSinkBootstrapServers);
        assertThat(config.kafkaSinkProperties())
                .containsEntry("linger.ms", "42")
                .containsEntry("batch.size", "111");
        assertThat(config.kafkaSourceStartTime()).isEqualTo(kafkaStartTime);
        assertThat(config.kafkaSourceEndTime()).isEqualTo(kafkaEndTime);
        assertThat(config.pipeline()).isEqualTo(pipeline);
        assertThat(config.fetchFailureTopic()).isEqualTo(fetchFailureTopic);
        assertThat(config.fetchSuccessTopic()).isEqualTo(fetchSuccessTopic);
        assertThat(config.clock().get().toEpochMilli())
                .isCloseTo(Instant.now().toEpochMilli(), Offset.offset(10L));

        assertThat(config.httpRoutes()).containsAllEntriesOf(httpRoutes);
    }

    @Test
    void fromRequiredArgs() {
        final ProducerConfig config =
                ProducerConfig.of(
                        ParameterTool.fromArgs(
                                new String[] {
                                    "--event-stream-config-url",
                                    streamConfig,
                                    "--event-stream-json-schema-urls",
                                    jsonSchemaUrl,
                                    "--kafka-source-config.bootstrap.servers",
                                    kafkaSourceBootstrapServers,
                                    "--kafka-source-config.group.id",
                                    kafkaSourceGroupId,
                                    "--page-change-stream",
                                    pageChangeStream,
                                    "--article-topic-stream",
                                    "unused-empty-stream",
                                    "--draft-topic-stream",
                                    "unused-empty-stream",
                                    "--recommendation-create-stream",
                                    "unused-empty-stream",
                                    "--pipeline",
                                    pipeline,
                                    "--fetch-failure-topic",
                                    fetchFailureTopic,
                                    "--fetch-success-topic",
                                    fetchSuccessTopic
                                }));

        assertThat(config.retryFetchMaxAge()).isEqualTo(Duration.ofSeconds(10));
        assertThat(config.eventStreamJsonSchemaUrls()).isEqualTo(singletonList(jsonSchemaUrl));
        assertThat(config.fetchRequestTimeout()).isEqualTo(Duration.ofSeconds(5));
        assertThat(config.kafkaSourceBootstrapServers()).isEqualTo(kafkaSourceBootstrapServers);
        assertThat(config.kafkaSinkBootstrapServers()).isEqualTo(kafkaSourceBootstrapServers);
        assertThat(config.pipeline()).isEqualTo(pipeline);
        assertThat(config.fetchFailureTopic()).isEqualTo(fetchFailureTopic);
        assertThat(config.fetchSuccessTopic()).isEqualTo(fetchSuccessTopic);
        assertThat(config.clock().get().toEpochMilli())
                .isCloseTo(getNowUTC().toEpochMilli(), Offset.offset(10L));
    }

    private static Instant getNowUTC() {
        return Instant.now(Clock.systemUTC());
    }
}
