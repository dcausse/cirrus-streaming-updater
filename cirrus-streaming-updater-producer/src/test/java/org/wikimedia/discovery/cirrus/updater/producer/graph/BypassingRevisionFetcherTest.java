package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

@ExtendWith(MockitoExtension.class)
class BypassingRevisionFetcherTest {

    @Mock RevisionFetcher delegate;

    @Mock InputEvent inputEvent;

    @InjectMocks BypassingRevisionFetcher bypassingRevisionFetcher;

    @Test
    void bypassDelete() throws IOException {
        when(inputEvent.getChangeType()).thenReturn(ChangeType.PAGE_DELETE);
        bypassingRevisionFetcher.apply(inputEvent);
        verifyNoInteractions(delegate);
    }

    @Test
    void delegate() {
        when(inputEvent.getChangeType()).thenReturn(ChangeType.REV_BASED_UPDATE);
        bypassingRevisionFetcher.apply(inputEvent);
        verify(delegate).apply(inputEvent);
    }

    @Test
    void close() throws IOException {
        bypassingRevisionFetcher.close();
        verify(delegate).close();
    }
}
