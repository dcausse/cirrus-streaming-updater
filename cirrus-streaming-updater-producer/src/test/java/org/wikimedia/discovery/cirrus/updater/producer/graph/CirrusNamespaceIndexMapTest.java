package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockExtension;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import org.apache.flink.streaming.api.operators.StreamMap;
import org.apache.flink.streaming.util.OneInputStreamOperatorTestHarness;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.testcontainers.shaded.com.google.common.collect.ImmutableMap;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;

class CirrusNamespaceIndexMapTest {
    public static final String API_CALL_URL =
            "/w/api.php?action=cirrus-config-dump&prop=replicagroup%7Cnamespacemap&format=json&formatversion=2";
    @RegisterExtension static WireMockExtension wireMockExtension = getWireMockExtension();

    static final String DOMAIN = "testwiki.local";
    static final String WIKI = "testwiki";

    static final String OTHER_DOMAIN = "otherwiki.local";
    static final String OTHER_WIKI = "otherwiki";
    private CirrusNamespaceIndexMap mapper;
    static final int TIMEOUT_MILLIS = 1000;
    private OneInputStreamOperatorTestHarness<InputEvent, InputEvent> testHarness;

    @BeforeEach
    void createMapper(WireMockRuntimeInfo info) throws Exception {
        mapper =
                new CirrusNamespaceIndexMap(
                        ImmutableMap.of(DOMAIN, info.getHttpBaseUrl(), OTHER_DOMAIN, info.getHttpBaseUrl()),
                        Duration.of(TIMEOUT_MILLIS, ChronoUnit.MILLIS));
        testHarness = new OneInputStreamOperatorTestHarness<>(new StreamMap<>(mapper));
        testHarness.open();
    }

    void happyPathScenario() {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("happy_path")
                        .willReturn(
                                WireMock.aResponse().withBodyFile("testwiki-namespace-map.json").withStatus(200)));
    }

    void twoWikisScenario() {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("two_wikis")
                        .willReturn(
                                WireMock.aResponse().withBodyFile("testwiki-namespace-map.json").withStatus(200)));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(OTHER_DOMAIN))
                        .inScenario("two_wikis")
                        .willReturn(
                                WireMock.aResponse().withBodyFile("otherwiki-namespace-map.json").withStatus(200)));
    }

    void unrecoverableFailureScenario() {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("timeout")
                        .willReturn(
                                WireMock.aResponse().withBodyFile("otherwiki-namespace-map.json").withStatus(429)));
    }

    void retryScenario() {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("retry")
                        .willSetStateTo("after first failure")
                        .willReturn(WireMock.aResponse().withStatus(429)));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("retry")
                        .whenScenarioStateIs("after first failure")
                        .willSetStateTo("after second failure")
                        .willReturn(WireMock.aResponse().withFault(Fault.RANDOM_DATA_THEN_CLOSE)));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("retry")
                        .whenScenarioStateIs("after second failure")
                        .willSetStateTo("after third failure")
                        .willReturn(WireMock.aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER)));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("retry")
                        .whenScenarioStateIs("after third failure")
                        .willSetStateTo("after fourth failure")
                        .willReturn(WireMock.aResponse().withFault(Fault.EMPTY_RESPONSE)));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("retry")
                        .whenScenarioStateIs("after fourth failure")
                        .willSetStateTo("after fourth failure")
                        .willReturn(
                                WireMock.aResponse().withBodyFile("testwiki-namespace-map.json").withStatus(200)));
    }

    void verifyNbCall(int nbCall, String hostname) {
        wireMockExtension.verify(
                WireMock.exactly(nbCall),
                WireMock.getRequestedFor(WireMock.urlEqualTo(API_CALL_URL))
                        .withHeader("host", WireMock.equalTo(hostname)));
    }

    @Test
    void test_happy_path() {
        happyPathScenario();
        InputEvent.TargetDocument t = mapper.map(createTestWikiInputEvent(0)).getTargetDocument();
        assertThat(t.getIndexName()).isEqualTo("testwiki_content");
        assertThat(t.getClusterGroup()).isEqualTo("omega");
        verifyNbCall(1, DOMAIN);
    }

    @Test
    void test_two_wikis() {
        twoWikisScenario();
        InputEvent.TargetDocument t = mapper.map(createTestWikiInputEvent(0)).getTargetDocument();
        assertThat(t.getIndexName()).isEqualTo("testwiki_content");
        assertThat(t.getClusterGroup()).isEqualTo("omega");

        t = mapper.map(createOtherWikiInputEvent(6)).getTargetDocument();
        assertThat(t.getIndexName()).isEqualTo("otherwiki_file");
        assertThat(t.getClusterGroup()).isEqualTo("psi");
        verifyNbCall(1, DOMAIN);
        verifyNbCall(1, OTHER_DOMAIN);
    }

    @Test
    void test_call_is_cached() {
        happyPathScenario();
        mapper.map(createTestWikiInputEvent(0));
        mapper.map(createTestWikiInputEvent(0));
        verifyNbCall(1, DOMAIN);
    }

    @Test
    void test_unknown_namespace() {
        happyPathScenario();
        InputEvent.TargetDocument t = mapper.map(createTestWikiInputEvent(10000)).getTargetDocument();
        assertThat(t.getIndexName()).isEqualTo("testwiki_general");
        assertThat(t.getClusterGroup()).isEqualTo("omega");
        verifyNbCall(1, DOMAIN);
    }

    @Test
    void test_no_api_call_is_made_when_everything_is_provided() {
        InputEvent input = createTestWikiInputEvent(0);
        input.getTargetDocument().completeWith("random_replica", "random_index_name");
        InputEvent.TargetDocument t = mapper.map(input).getTargetDocument();

        assertThat(t.getIndexName()).isEqualTo("random_index_name");
        assertThat(t.getClusterGroup()).isEqualTo("random_replica");
        verifyNbCall(0, DOMAIN);
    }

    @Test
    @Timeout(5)
    void test_unrecoverable_failure() {
        unrecoverableFailureScenario();
        InputEvent e = createTestWikiInputEvent(0);
        assertThatThrownBy(() -> mapper.map(e))
                .isInstanceOf(IllegalStateException.class)
                .hasCauseInstanceOf(InvalidMWApiResponseException.class);
        verifyNbCall(5, DOMAIN);
    }

    @Test
    @Timeout(5)
    void test_retry() {
        retryScenario();
        InputEvent.TargetDocument t = mapper.map(createTestWikiInputEvent(0)).getTargetDocument();

        assertThat(t.getIndexName()).isEqualTo("testwiki_content");
        assertThat(t.getClusterGroup()).isEqualTo("omega");
        verifyNbCall(5, DOMAIN);
    }

    InputEvent createTestWikiInputEvent(long namespace) {
        InputEvent inputEvent = new InputEvent();
        inputEvent.setTargetDocument(new InputEvent.TargetDocument(DOMAIN, WIKI, namespace, 1L));
        return inputEvent;
    }

    InputEvent createOtherWikiInputEvent(long namespace) {
        InputEvent inputEvent = new InputEvent();
        inputEvent.setTargetDocument(
                new InputEvent.TargetDocument(OTHER_DOMAIN, OTHER_WIKI, namespace, 1L));
        return inputEvent;
    }
}
