package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.ProcessingTimeTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.runtime.operators.windowing.WindowOperator;
import org.apache.flink.streaming.runtime.operators.windowing.functions.InternalIterableProcessWindowFunction;
import org.apache.flink.streaming.runtime.streamrecord.StreamRecord;
import org.apache.flink.streaming.util.KeyedOneInputStreamOperatorTestHarness;
import org.apache.flink.streaming.util.OneInputStreamOperatorTestHarness;
import org.apache.flink.streaming.util.TestHarnessUtil;
import org.junit.Test;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

public class DeduplicateAndMergeTest {
    private static final byte[] MOCK_RAW_FIELDS = "{}".getBytes(StandardCharsets.UTF_8);
    private static final InputEvent.Update MOCK_UPDATE =
            new InputEvent.Update(Collections.emptyMap(), MOCK_RAW_FIELDS);
    InputEvent.TargetDocument target = new InputEvent.TargetDocument("domain", "wikiId", 0L, 1L);
    Instant eventTime = Instant.EPOCH;

    @Test
    public void doesNothingWithOneEvent() throws Exception {
        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(Lists.newArrayList("foo/bar|1"))));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactlyInAnyOrderElementsOf(inputs);
    }

    @Test
    public void mergesTwoWeightedTags() throws Exception {
        List<InputEvent> results =
                process(
                        Lists.newArrayList(
                                newInputEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        InputEvent.Update.forWeightedTags(Lists.newArrayList("foo/exists|1"))),
                                newInputEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        InputEvent.Update.forWeightedTags(Lists.newArrayList("bar/exists|1")))));
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrder("foo/exists|1", "bar/exists|1");
    }

    @Test
    public void mergesManyWeightedTags() throws Exception {
        List<InputEvent> results =
                process(
                        Lists.newArrayList(
                                newInputEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        InputEvent.Update.forWeightedTags(Lists.newArrayList("exists|1"))),
                                newInputEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        InputEvent.Update.forWeightedTags(Lists.newArrayList("bar/a|1", "bar/b|1"))),
                                newInputEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        InputEvent.Update.forWeightedTags(Lists.newArrayList("qqq/q|2")))));
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrder("exists|1", "bar/a|1", "bar/b|1", "qqq/q|2");
    }

    @Test
    public void newestTagWinsWithOverlappingWeightedTagsForSameRevision() throws Exception {
        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(Lists.newArrayList("foo/a|1"))),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(Lists.newArrayList("foo/b|1"))));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
    }

    @Test
    public void mergesWeightedTagsIntoUpdates() throws Exception {
        List<String> weightedTags = Lists.newArrayList("foo/exists|1");
        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE, 1L, InputEvent.Update.forWeightedTags(weightedTags)));

        List<InputEvent> results = process(inputs);
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrderElementsOf(weightedTags);
        assertThat(results.get(0).getUpdate().getRawFields()).isEqualTo(MOCK_UPDATE.getRawFields());
    }

    @Test
    public void mergesWeightedTagsForOlderRevisionIntoUpdates() throws Exception {
        List<String> weightedTags = Lists.newArrayList("foo/exists|1");
        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(
                                ChangeType.TAGS_UPDATE, 1L, InputEvent.Update.forWeightedTags(weightedTags)),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 2L, MOCK_UPDATE));
        List<InputEvent> results = process(inputs);
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrderElementsOf(weightedTags);
    }

    @Test
    public void dropsOverlappingWeightedTagsForOlderRevision() throws Exception {
        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(Lists.newArrayList("foo/a|1"))),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                2L,
                                InputEvent.Update.forWeightedTags(Lists.newArrayList("foo/b|1"))));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));

        // Same, but with reversed event time ordering
        inputs =
                Lists.newArrayList(
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                2L,
                                InputEvent.Update.forWeightedTags(Lists.newArrayList("foo/b|1"))),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(Lists.newArrayList("foo/a|1"))));
        results = process(inputs);
        assertThat(results).containsExactly(inputs.get(0));
    }

    @Test
    public void mergesMultipleWeightedTagsIntoUpdates() throws Exception {
        List<InputEvent> results =
                process(
                        Lists.newArrayList(
                                newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                                newInputEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        InputEvent.Update.forWeightedTags(Lists.newArrayList("foo/exists|1"))),
                                newInputEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        InputEvent.Update.forWeightedTags(Lists.newArrayList("bar/exists|1")))));
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrder("foo/exists|1", "bar/exists|1");
        assertThat(results.get(0).getUpdate().getRawFields()).isEqualTo(MOCK_UPDATE.getRawFields());
    }

    @Test
    public void emitsHighestRevisionForMultipleRevisions() throws Exception {
        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 2L, MOCK_UPDATE));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));

        // ingesting in reverse of event time shouldn't matter
        Collections.reverse(inputs);
        results = process(inputs);
        assertThat(results).containsExactly(inputs.get(0));

        // the older revision having a newer event time shouldn't matter
        inputs =
                Lists.newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 2L, MOCK_UPDATE),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE));
        results = process(inputs);
        assertThat(results).containsExactly(inputs.get(0));
    }

    @Test
    public void emitsMostRecentEventForSameRevision() throws Exception {
        List<InputEvent> inputs =
                Lists.newArrayList(
                        // note that event time increases on each call to newInputEvent
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE));

        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));

        // ingesting in reverse of event time shouldn't matter
        Collections.reverse(inputs);
        results = process(inputs);
        assertThat(results).containsExactly(inputs.get(0));
    }

    @Test
    public void dedpulicatesRevBasedUpdates() throws Exception {
        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
    }

    @Test
    public void passesThroughDeletes() throws Exception {
        List<InputEvent> inputs = Lists.newArrayList(newInputEvent(ChangeType.PAGE_DELETE, 1L, null));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactlyInAnyOrderElementsOf(inputs);
    }

    @Test
    public void dropsTagUpdatesWhenDeleted() throws Exception {
        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(Lists.newArrayList("example/a|1"))),
                        newInputEvent(ChangeType.PAGE_DELETE, 2L, null),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(Lists.newArrayList("other/a|1"))));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
    }

    @Test
    public void dropsOldRevUpdatesWhenDeleted() throws Exception {
        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(ChangeType.PAGE_DELETE, 2L, null));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
    }

    @Test
    public void dropsDeletePriorToUndelete() throws Exception {
        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(ChangeType.PAGE_DELETE, 1L, null),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
    }

    @Test
    public void deduplicatesDeletes() throws Exception {
        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(ChangeType.PAGE_DELETE, 1L, null),
                        newInputEvent(ChangeType.PAGE_DELETE, 1L, null));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
    }

    @Test
    public void mergesHintsWhenEqual() throws Exception {
        Map<String, String> hint = new HashMap<>();
        hint.put("field", "value");
        hint.put("weighted_tags", "multilist");

        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(
                                ChangeType.REV_BASED_UPDATE, 1L, new InputEvent.Update(hint, MOCK_RAW_FIELDS)),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(Lists.newArrayList("example/a|1"))));
        List<InputEvent> outputs = process(inputs);
        assertThat(outputs)
                .extracting(r -> r.getUpdate().getNoopHints())
                .containsExactlyInAnyOrder(hint);
    }

    @Test
    public void mergesNonOverlapingHints() throws Exception {
        List<InputEvent> inputs =
                Lists.newArrayList(
                        newInputEvent(
                                ChangeType.REV_BASED_UPDATE,
                                1L,
                                new InputEvent.Update(Collections.singletonMap("field", "value"), MOCK_RAW_FIELDS)),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(Lists.newArrayList("foo/a|1"))));
        List<InputEvent> outputs = process(inputs);
        assertThat(outputs)
                .extracting(r -> r.getUpdate().getNoopHints())
                .containsExactlyInAnyOrder(
                        ImmutableMap.of(
                                "field", "value",
                                "weighted_tags", "multilist"));
    }

    private InputEvent newInputEvent(ChangeType changeType, Long revId, InputEvent.Update update) {
        InputEvent e = new InputEvent();
        e.setChangeType(changeType);
        e.setUpdate(update);
        e.setTargetDocument(target);
        e.setRevId(revId);
        // Make event time ordering the same as creation order.
        e.setEventTime(eventTime);
        eventTime = eventTime.plus(Duration.ofMinutes(1));
        return e;
    }

    private static OneInputStreamOperatorTestHarness<InputEvent, InputEvent> createTestHarness()
            throws Exception {
        final int windowSize = 3;
        TypeInformation<Tuple2<String, Long>> keyTypeInfo =
                TypeInformation.of(new TypeHint<Tuple2<String, Long>>() {});
        ListStateDescriptor<InputEvent> stateDesc =
                new ListStateDescriptor<>(
                        "window-contents",
                        TypeInformation.of(InputEvent.class).createSerializer(new ExecutionConfig()));
        WindowOperator<Tuple2<String, Long>, InputEvent, Iterable<InputEvent>, InputEvent, TimeWindow>
                operator =
                        new WindowOperator<>(
                                TumblingProcessingTimeWindows.of(Time.seconds(windowSize)),
                                new TimeWindow.Serializer(),
                                DeduplicateAndMerge.keySelector(),
                                keyTypeInfo.createSerializer(new ExecutionConfig()),
                                stateDesc,
                                new InternalIterableProcessWindowFunction<>(new DeduplicateAndMerge<>()),
                                ProcessingTimeTrigger.create(),
                                0,
                                null);

        return new KeyedOneInputStreamOperatorTestHarness<>(
                operator, operator.getKeySelector(), keyTypeInfo);
    }

    private List<InputEvent> process(Collection<InputEvent> collection) throws Exception {
        // We don't really use the full test harness, but something has to be inplace for the metrics
        // to not blow up everything else. Could probably use a less complete mock, but this is
        // available and works well enough.
        try (OneInputStreamOperatorTestHarness<InputEvent, InputEvent> testHarness =
                createTestHarness()) {
            testHarness.open();
            // Note that processing time is in ms
            testHarness.setProcessingTime(5);
            for (InputEvent event : collection) {
                testHarness.processElement(new StreamRecord<>(event));
            }
            // Necessary to close the windows
            testHarness.setProcessingTime(5000);
            return TestHarnessUtil.getRawElementsFromOutput(testHarness.getOutput());
        }
    }
}
