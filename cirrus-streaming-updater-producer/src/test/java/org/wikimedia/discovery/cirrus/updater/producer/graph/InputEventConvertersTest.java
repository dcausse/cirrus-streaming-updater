package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.time.Instant;

import javax.annotation.Nonnull;

import org.apache.commons.io.IOUtils;
import org.apache.flink.types.Row;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.serde.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

class InputEventConvertersTest {
    private final RevisionFetcher.EventAugmenter augmenter =
            InputEventConverters.cirrusDocAugmenter(new ObjectMapper());

    @Test
    void test_revision_create_conversion() throws IOException {
        EventDataStreamFactory factory =
                EventDataStreamFactory.from(
                        singletonList("https://schema.wikimedia.org/repositories/primary/jsonschema"),
                        this.getClass().getResource("/event-stream-config.json").toString());
        byte[] eventData =
                IOUtils.toByteArray(
                        getClass().getResourceAsStream(getClass().getSimpleName() + "/page_change-event.json"));
        Row event = factory.deserializer("rc0.mediawiki.page_change").deserialize(eventData);
        InputEvent inputEvent = InputEventConverters.fromPageChange(event);
        InputEvent.TargetDocument target = inputEvent.getTargetDocument();
        assertThat(inputEvent.getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        assertThat(inputEvent.getEventTime()).isEqualTo(Instant.parse("2022-11-22T15:28:50Z"));
        assertThat(inputEvent.getEventStream()).isEqualTo("rc0.mediawiki.page_change");
        assertThat(target.getWikiId()).isEqualTo("testwiki");
        assertThat(inputEvent.getRequestId()).isEqualTo("6d7355de-d623-4dcc-9691-ad58ecafe5d1");
        assertThat(target.getPageNamespace()).isEqualTo(2);
        assertThat(target.getPageId()).isEqualTo(147498);
        assertThat(inputEvent.getRevId()).isEqualTo(551141);
        assertThat(inputEvent.getIngestionTime()).isNull();
    }

    @Test
    void happyPath() throws IOException {
        final Instant now = Instant.now();
        final InputEvent input = createInputEvent(now);
        final InputEvent output =
                augmenter.augmentWithCirrusBuildDoc(
                        input,
                        EventDataStreamUtilities.parseJson("/wiremock/__files/revision.551141.json")
                                .at("/query/pages/0"));

        InputEvent.TargetDocument target = output.getTargetDocument();
        assertThat(output.getEventTime()).isEqualTo(now);
        assertThat(output.getRevId()).isEqualTo(551141L);
        assertThat(target.getPageId()).isEqualTo(147498L);
        assertThat(target.getDomain()).isEqualTo("my.domain.local");
        assertThat(target.getWikiId()).isEqualTo("mywiki_id");
        assertThat(target.getIndexName()).isEqualTo("testwiki_general");
        assertThat(target.getClusterGroup()).isEqualTo("omega");
        assertThat(target.isComplete()).isTrue();
    }

    @Test
    void augmentFailsIfInputIsIncomplete() {
        assertThatThrownBy(
                        () -> {
                            augmenter.augmentWithCirrusBuildDoc(
                                    createInputEvent(Instant.now()),
                                    EventDataStreamUtilities.parseJson(
                                            "/wiremock/__files/revision.551141.no-metadata.json"));
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void augmentFailsIfPageIdDoesNotMatch() {
        final InputEvent inputEvent = createInputEvent(Instant.now());
        InputEvent.TargetDocument old = inputEvent.getTargetDocument();
        inputEvent.setTargetDocument(
                new InputEvent.TargetDocument(
                        old.getDomain(), old.getWikiId(), old.getPageNamespace(), -1L));

        assertThatThrownBy(
                        () -> {
                            augmenter.augmentWithCirrusBuildDoc(
                                    inputEvent,
                                    EventDataStreamUtilities.parseJson("/wiremock/__files/revision.551141.json")
                                            .at("/query/pages/0"));
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void augmentFailsIfRevIdDoesNotMatch() {
        final InputEvent inputEvent = createInputEvent(Instant.now());
        inputEvent.setRevId(-1L);

        assertThatThrownBy(
                        () -> {
                            augmenter.augmentWithCirrusBuildDoc(
                                    inputEvent,
                                    EventDataStreamUtilities.parseJson("/wiremock/__files/revision.551141.json")
                                            .at("/query/pages/0"));
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Nonnull
    private static InputEvent createInputEvent(Instant now) {
        final InputEvent inputEvent = new InputEvent();
        inputEvent.setEventTime(now);
        inputEvent.setRevId(551141L);
        inputEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
        inputEvent.setTargetDocument(
                new InputEvent.TargetDocument("my.domain.local", "mywiki_id", 0L, 147498L));
        return inputEvent;
    }
}
