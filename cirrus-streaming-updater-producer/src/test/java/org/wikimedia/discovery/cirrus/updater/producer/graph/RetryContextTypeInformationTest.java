package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.INT_TYPE_INFO;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.NoSuchElementException;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.apache.flink.api.java.typeutils.runtime.TestDataOutputSerializer;
import org.apache.flink.core.memory.DataInputDeserializer;
import org.junit.jupiter.api.Test;

class RetryContextTypeInformationTest {

    @Test
    void serde() throws IOException {
        final PojoTypeInfo<RetryContext<Integer, Integer>> typeInformation =
                RetryContextTypeInformation.create(INT_TYPE_INFO, INT_TYPE_INFO);

        final TypeSerializer<RetryContext<Integer, Integer>> serializer =
                typeInformation.createSerializer(new ExecutionConfig());

        final RetryContext<Integer, Integer> originalContext =
                new RetryContext<>(42, 1, 2, new NoSuchElementException());

        final TestDataOutputSerializer target = new TestDataOutputSerializer(4096);
        serializer.serialize(originalContext, target);

        final RetryContext<Integer, Integer> restoredContext =
                serializer.deserialize(new DataInputDeserializer(target.copyByteBuffer()));

        assertThat(originalContext.getInput()).isEqualTo(restoredContext.getInput());
        assertThat(originalContext.getOutput()).isEqualTo(restoredContext.getOutput());
        assertThat(originalContext.getIteration()).isEqualTo(restoredContext.getIteration());
        assertThat(originalContext.getFailure()).hasSameClassAs(restoredContext.getFailure());
    }
}
