package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.time.Duration;
import java.time.Instant;
import java.util.stream.Stream;

import org.junit.jupiter.api.Named;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

class RevisionFetcherRetryPredicateTest {

    static final Instant NOW = Instant.parse("2022-01-01T10:00:00.000Z");

    @ParameterizedTest
    @MethodSource("parameters")
    void trueForInTimeRetryable(boolean expected, InputEvent input, int iteration, Throwable error) {
        RevisionFetcherRetryPredicate predicate = buildPredicate();
        assertThat(predicate.test(RetryContext.of(iteration, input, null, error))).isEqualTo(expected);
    }

    static Stream<Arguments> parameters() {
        final Instant inTimeRevTimestamp = NOW.minusSeconds(1);
        final Instant lateRevTimestamp = NOW.minusSeconds(15);

        return Stream.of(
                Arguments.of(
                        Named.of("in-time, retryable", true),
                        Named.of("input " + inTimeRevTimestamp, buildInputEvent(inTimeRevTimestamp)),
                        Named.of("iteration " + 0, 0),
                        new IOException()),
                Arguments.of(
                        Named.of("in-time, retryable", true),
                        Named.of("input " + inTimeRevTimestamp, buildInputEvent(inTimeRevTimestamp)),
                        Named.of("iteration " + 0, 0),
                        new InvalidMWApiResponseException("")),
                Arguments.of(
                        Named.of("in-time, retryable", true),
                        Named.of("input " + inTimeRevTimestamp, buildInputEvent(inTimeRevTimestamp)),
                        Named.of("iteration " + 0, 0),
                        new SocketTimeoutException()),
                Arguments.of(
                        Named.of("in-time, retryable", true),
                        Named.of("input " + inTimeRevTimestamp, buildInputEvent(inTimeRevTimestamp)),
                        Named.of("iteration " + 0, 0),
                        new RevisionNotFoundException("")),
                Arguments.of(
                        Named.of("in-time, non-retryable", false),
                        Named.of("input " + inTimeRevTimestamp, buildInputEvent(inTimeRevTimestamp)),
                        Named.of("iteration " + 1, 1),
                        new OutOfMemoryError()),
                Arguments.of(
                        Named.of("late, retryable", true),
                        Named.of("input " + lateRevTimestamp, buildInputEvent(lateRevTimestamp)),
                        Named.of("iteration " + 2, 2),
                        new IOException()),
                Arguments.of(
                        Named.of("late > max retries, retryable", false),
                        Named.of("input " + lateRevTimestamp, buildInputEvent(lateRevTimestamp)),
                        Named.of("iteration " + 3, 3),
                        new IOException()));
    }

    private static InputEvent buildInputEvent(Instant revTimestamp) {
        InputEvent inputEvent = new InputEvent();
        inputEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
        inputEvent.setEventTime(revTimestamp);
        return inputEvent;
    }

    private RevisionFetcherRetryPredicate buildPredicate() {
        return new RevisionFetcherRetryPredicate(Duration.ofSeconds(10), () -> NOW, 3);
    }
}
