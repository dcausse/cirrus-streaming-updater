package org.wikimedia.discovery.cirrus.updater.producer.sink;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Collections;
import java.util.stream.Stream;

import javax.annotation.Nonnull;

import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.java.typeutils.runtime.TestDataOutputSerializer;
import org.apache.flink.types.Row;
import org.assertj.core.api.Condition;
import org.junit.Test;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.common.serde.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

import com.fasterxml.jackson.databind.ObjectMapper;

class UpdateRowEncoderTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final EventRowTypeInfo outputTypeInfo = EventDataStreamUtilities.buildUpdateTypeInfo();
    private final UpdateRowEncoder encoder = new UpdateRowEncoder(outputTypeInfo);

    @Test
    void encodeAndSerialize() throws IOException {
        final Instant now = Instant.now();

        final InputEvent inputEvent = createInputEvent(now);
        inputEvent.getTargetDocument().completeWith("omega", "testwiki_general");
        inputEvent.setUpdate(
                new InputEvent.Update(Collections.emptyMap(), "{}".getBytes(StandardCharsets.UTF_8)));
        final Row row = encoder.encodeInputEvent(inputEvent);

        assertThat(row.getField("dt")).isEqualTo(now);
        assertThat(row.getField("page_id")).isEqualTo(147498L);
        assertThat(row.getField("rev_id")).isEqualTo(551141L);
        assertThat(row.getField("domain")).isEqualTo("my.domain.local");
        assertThat(row.getField("wiki_id")).isEqualTo("mywiki_id");
        assertThat(row.getField("index_name")).isEqualTo("testwiki_general");
        assertThat(row.getField("cluster_group")).isEqualTo("omega");

        final TypeSerializer<Row> rowSerializer = outputTypeInfo.createSerializer(null);
        final TestDataOutputSerializer target = new TestDataOutputSerializer(4096);
        rowSerializer.serialize(row, target);

        assertThat(target.length()).isPositive();
    }

    @ParameterizedTest
    @MethodSource("encodeInputEventParameters")
    void encodeInputEvent(InputEvent inputEvent, Condition<Row> condition) throws IOException {
        assertThat(encoder.encodeInputEvent(inputEvent)).has(condition);
    }

    static Stream<Arguments> encodeInputEventParameters() {
        final Instant now = Instant.now();
        final InputEvent delete = createInputEvent(now);
        delete.setChangeType(ChangeType.PAGE_DELETE);
        delete.getTargetDocument().completeWith("clusterGroup", "indexName");
        final InputEvent revBasedUpdate = createInputEvent(now);
        revBasedUpdate.setChangeType(ChangeType.REV_BASED_UPDATE);
        revBasedUpdate.getTargetDocument().completeWith("clusterGroup", "indexName");
        revBasedUpdate.setUpdate(
                new InputEvent.Update(Collections.emptyMap(), "{}".getBytes(StandardCharsets.UTF_8)));

        return Stream.of(
                Arguments.of(
                        Named.of(UpdateFields.OPERATION_UPDATE_REVISION, revBasedUpdate),
                        new Condition<Row>(
                                row ->
                                        UpdateFields.getOperation(row).equals(UpdateFields.OPERATION_UPDATE_REVISION),
                                "Unexpected operation")),
                Arguments.of(
                        Named.of(UpdateFields.OPERATION_DELETE, delete),
                        new Condition<Row>(
                                row -> UpdateFields.getOperation(row).equals(UpdateFields.OPERATION_DELETE),
                                "Unexpected operation")));
    }

    @Nonnull
    private static InputEvent createInputEvent(Instant now) {
        final InputEvent inputEvent = new InputEvent();
        inputEvent.setEventTime(now);
        inputEvent.setTargetDocument(
                new InputEvent.TargetDocument("my.domain.local", "mywiki_id", 0L, 147498L));
        inputEvent.setRevId(551141L);
        inputEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
        return inputEvent;
    }
}
