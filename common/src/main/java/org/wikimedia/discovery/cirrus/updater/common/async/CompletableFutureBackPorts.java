package org.wikimedia.discovery.cirrus.updater.common.async;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;

/** Java 9 back-ports for {@link java.util.concurrent.CompletableFuture} utility functions. */
@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
@SuppressFBWarnings(
        value = "HideUtilityClassConstructor",
        justification = "lombok takes care of that")
public class CompletableFutureBackPorts {

    /**
     * Returns a new CompletionStage that is already completed with the given value.
     *
     * <p>Backport from Java 9.
     *
     * @param value the value
     * @return the completed CompletionStage
     * @param <T> the type of {@code value}
     * @see CompletableFuture#completedStage(Object)
     */
    public static <T> CompletionStage<T> completedFuture(T value) {
        final CompletableFuture<T> result = new CompletableFuture<>();
        result.complete(value);
        return result;
    }

    /**
     * Returns a new CompletionStage that is already failed with the given exception.
     *
     * <p>Backport from Java 9.
     *
     * @param exception the exception
     * @return the completed CompletionStage
     * @param <T> the type of {@code value}
     * @see CompletableFuture#failedFuture(Throwable)
     */
    public static <T> CompletionStage<T> failedFuture(Throwable exception) {
        final CompletableFuture<T> result = new CompletableFuture<>();
        result.completeExceptionally(exception);
        return result;
    }
}
