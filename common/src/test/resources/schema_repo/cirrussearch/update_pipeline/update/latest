title: cirrussearch/update_pipeline/update
description: Internal event for page updates handled by CirrusSearch
$id: /cirrussearch/update_pipeline/update/1.0.0
$schema: 'https://json-schema.org/draft-07/schema#'
type: object
required:
  - $schema
  - dt
  - meta
  - operation
  - page_id
  - page_namespace
  - rev_id
properties:
  $schema:
    description: >
      A URI identifying the JSONSchema for this event. This should match an
      schema's $id in a schema repository. E.g. /schema/title/1.0.0
    type: string
  dt:
    description: 'UTC event datetime in ISO-8601 format'
    type: string
    format: date-time
    maxLength: 128
  meta:
    type: object
    required:
      - dt
      - stream
    properties:
      domain:
        description: Domain the event or entity pertains to
        type: string
        minLength: 1
      dt:
        description: 'UTC event datetime in ISO-8601 format'
        type: string
        format: date-time
        maxLength: 128
      id:
        description: Unique ID of this event
        type: string
      request_id:
        description: Unique ID of the request that caused the event
        type: string
      stream:
        description: Name of the stream/queue/dataset that this event belongs in
        type: string
        minLength: 1
      uri:
        description: Unique URI identifying the event or entity
        type: string
        format: uri-reference
        maxLength: 8192
  operation:
    type: string
    enum:
      - delete
      - page_refresh
      - update_fragment
      - update_revision
  page_id:
    description: The page ID of the page this revision belongs to.
    type: integer
    maximum: 9223372036854775808
    minimum: 1
  page_namespace:
    type: integer
    maximum: 9223372036854775808
    minimum: 0
  wiki_id:
    description: ID of the wiki project
    type: string
    minLength: 1
  domain:
    description: Domain of the wiki project
    type: string
    minLength: 1
  rev_id:
    description: The (database) revision ID.
    type: integer
    maximum: 9223372036854775808
    minimum: 0
  index_name:
    description: The name of the index to write to.
    type: string
    minLength: 1
  cluster_group:
    type: string
  noop_hints:
    type: object
    additionalProperties:
      type: string
  fields:
    description: intermediate document from cirrus search for elastic search
    type: object
    required:
      - page_id
      - version
    properties:
      auxiliary_text:
        type: array
        items:
          type: string
      category:
        type: array
        items:
          type: string
      content_model:
        type: string
      coordinates:
        type: array
        items:
          type: object
          properties:
            coord:
              type: object
              additionalProperties:
                type: number
            country:
              type: string
            dim:
              type: integer
              maximum: 9223372036854775808
              minimum: 0
            globe:
              type: string
            name:
              type: string
            primary:
              type: boolean
            region:
              type: string
            type:
              type: string
      create_timestamp:
        description: 'UTC event datetime in ISO-8601 format'
        type: string
        format: date-time
        maxLength: 128
      defaultsort:
        type: string
      descriptions:
        type: object
        additionalProperties:
          type: array
          items:
            type: string
      display_title:
        type: string
      external_link:
        type: array
        items:
          type: string
      extra_source:
        type: string
      file_bits:
        type: integer
        maximum: 9223372036854775808
        minimum: 0
      file_height:
        type: integer
        maximum: 9223372036854775808
        minimum: 0
      file_media_type:
        type: string
      file_mime:
        type: string
      file_resolution:
        type: integer
        maximum: 9223372036854775808
        minimum: 0
      file_size:
        type: integer
        maximum: 9223372036854775808
        minimum: 0
      file_text:
        type: string
      file_width:
        type: integer
        maximum: 9223372036854775808
        minimum: 0
      heading:
        type: array
        items:
          type: string
      incoming_links:
        type: integer
        maximum: 9223372036854775808
        minimum: 0
      labels:
        type: object
        additionalProperties:
          type: array
          items:
            type: string
      language:
        type: string
      local_sites_with_dupe:
        type: array
        items:
          type: string
      namespace:
        type: integer
        maximum: 9223372036854775808
        minimum: 0
      namespace_text:
        type: string
      opening_text:
        type: string
      outgoing_link:
        type: array
        items:
          type: string
      page_id:
        description: The page ID of the page this revision belongs to.
        type: integer
        maximum: 9223372036854775808
        minimum: 1
      popularity_score:
        type: number
      redirect:
        type: array
        items:
          type: object
          required:
            - namespace
            - title
          properties:
            namespace:
              type: integer
              maximum: 9223372036854775808
              minimum: 0
            title:
              type: string
      snapshot:
        type: string
      source_text:
        type: string
      statement_keywords:
        type: array
        items:
          type: string
      template:
        type: array
        items:
          type: string
      text:
        type: string
      text_bytes:
        type: integer
        maximum: 9223372036854775808
        minimum: 0
      timestamp:
        description: 'UTC event datetime in ISO-8601 format'
        type: string
        format: date-time
        maxLength: 128
      title:
        type: string
      version:
        description: The (database) revision ID.
        type: integer
        maximum: 9223372036854775808
        minimum: 0
      weighted_tags:
        type: array
        items:
          type: string
      wiki:
        type: string
      wikibase_item:
        type: string
